<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<html>
<head>
<title>Expressions</title>
</head>
<body>
	Converting a string "hello world" to upper-case <%= new String("Hello World").toUpperCase() %> <br><br>
	
	25 multiplied by 4 equals <%= 25*4 %> <br><br>
	
	is 75 < 69 ? <%= 75 < 69 %>
</body>
</html>