<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Training portal</title>
</head>
<body>
	<%
		String favLang = "Java";
	
		Cookie[] cookies = request.getCookies();
		
		if(cookies != null) {
			for (Cookie temp: cookies) {
				if("myFavProgLang".equals(temp.getName())) {
					favLang = temp.getValue();
				}
			}
		}
	%>
	<h4>New Books for <%= favLang %></h4>
	<h4>New Jobs for <%= favLang %></h4>
	
</body>
</html>