<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>personalize-response</title>
</head>
<body>
	<%
	String favLang = request.getParameter("p_languages");

	Cookie theCookie = new Cookie("myFavProgLang",favLang);
	
	theCookie.setMaxAge(60*60*24*30);
	
	response.addCookie(theCookie);
	%>
	Thanks! programming language set to: ${param.p_languages}
	<br><br>
	<a href="cookies-hompage.jsp">Homepage --></a>
</body>
</html>