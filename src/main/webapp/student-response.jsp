<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	Welcome: ${param.first_name } ${param.last_name}<br>
	From: ${param.country}<br>
	Selected Programming language: ${param.p_language}<br>
	languages: <ul>
		<% 
		String[] langs = request.getParameterValues("language");
		if(langs != null) {
			for (String name: langs) {
				out.print("<li>"+name+ "</li>");
			}
		} else {
				out.println("no language selected");
			}
		%>
	</ul>
</body>
</html>