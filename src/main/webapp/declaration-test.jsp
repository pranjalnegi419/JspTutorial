<html>
<head>
<title>Declarations</title>
</head>
<body>
	<%!
	String makeItLower(String text) {
		return text.toLowerCase();
	}
	%>
	make "HELLO" lower: <%= makeItLower("HELLO") %>
	<!--
	Don't declare too much in JSP declarations.
	Refactor code into seperate Java class(make use of mvc).
	
	-->
</body>
</html>