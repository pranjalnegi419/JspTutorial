<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import= "java.util.*"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<!-- create html form -->
	<form action="todo_demo.jsp">
		Add new Item: <input type="text" name="theItem" />
		
		<input type="submit" value="submit">
	</form>
		
	<!-- add new items to "To do" list -->
	<%
		// get the to do list from session
		List<String> items = (List<String>) session.getAttribute("myTodoList");
		// if the todo items doesnt exist, then create a new one
		if(items == null) {
			items = new ArrayList<String>();
			session.setAttribute("myTodoList",items);	
		}

		// see if there is form data to add
		String theItem = request.getParameter("theItem");
		boolean isNotEmpty= theItem != null && theItem.trim().length() > 0;
		boolean isNotDuplicate = theItem != null && !items.contains(theItem.trim());
		if(isNotEmpty && isNotDuplicate) {
			items.add(theItem);
		}
	%>
	<hr>
	<!-- Display all "to do" list from session -->
	<b>To do list:</b> <br>
		<%
		for(String item: items) {
			out.println(item + "<br>");
		}
		%>
</body>
</html>